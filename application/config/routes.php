<?php
defined('BASEPATH') OR exit('No direct script access allowed');



$route['default_controller'] = 'site';

$route['login'] = 'site/login';
$route['sign/account'] = 'site/account_form';
$route['home'] = 'site/home';
$route['coupon/new'] = 'site/new_coupon';

$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;
